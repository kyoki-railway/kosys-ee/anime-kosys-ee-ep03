
#############################################################################
# マクロ定義
#############################################################################

# 共通マクロ
include Makefile.inc


# サブディレクトリ 
SUBDIRS:=video

# エンコード関連
AVS2WAV:=$(ENC_UTIL_DIR)/avs2wav.exe
FFMPEG:=ffmpeg
X264:=$(ENC_UTIL_DIR)/x264_x64.exe
MUXER:=$(ENC_UTIL_DIR)/muxer.exe
REMUXER:=$(ENC_UTIL_DIR)/remuxer.exe

AOPTION=-c:a aac -b:a 512k

VOPTION_COMMON= --preset slower --me umh --bframes 3 --keyint 240 --min-keyint 23 --qcomp 0.70 --rc-lookahead 240 --scenecut 65 --no-dct-decimate --no-fast-pskip --aq-mode 1 --aq-strength 0.8
	# 注： bframesは3より大きいと黒背景白文字のスライドの動き（EDロールなど）で破綻する
	#      scenecutはアニメではデフォルトより大きめにしておくと良いらしい

VOPTION=     $(VOPTION_COMMON) --crf 16  --bluray-compat --vbv-maxrate 40000 --vbv-bufsize 30000 --level 4.1 --keyint 24 --slices 4 --colorprim "bt709" --transfer "bt709" --colormatrix "bt709" --sar 1:1 --b-pyramid strict --weightp 0
VOPT_FAST=--analyse none --subme 1 --me dia --crf 12
VOPT_SLOW=
FPS=24

IN_AVS=main.avs
VER_AVS=version.avs
OUT_MP4=_release/kosys_ee_ep03.mp4
TMPA1=_release/kosys_ee_ep03.tmp.m4a
TMPV1=_release/kosys_ee_ep03.tmp.264
TMPV2=_release/kosys_ee_ep03.tmp.mp4
OUT_MP4_PRE=_release/kosys_ee_ep03_pre.mp4
TMPV1_PRE=_release/kosys_ee_ep03_pre.tmp.264
TMPV2_PRE=_release/kosys_ee_ep03_pre.tmp.mp4

#############################################################################
# Makeルール
#############################################################################

.PHONY: all $(SUBDIRS)
all: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@


.PHONY: movie
movie: version $(OUT_MP4)
$(OUT_MP4): $(TMPA1) $(TMPV1) $(TMPV2)
	$(REMUXER) -i $(TMPV2) -i $(TMPA1) -o $(OUT_MP4)

$(TMPV2): $(TMPV1)
	$(MUXER) -i $(TMPV1)?fps=$(FPS) -o $(TMPV2)


$(TMPV1): $(wildcard *.avs) $(wildcard video/*.avs) $(wildcard video/_output/*.avi) $(VER_AVS)
	$(X264) $(VOPTION) $(VOPT_SLOW) -o $(TMPV1) $(IN_AVS)
	

$(TMPA1): $(wildcard audio/*.avs) $(wildcard audio/*.wav) $(wildcard audio/*.flac) $(wildcard audio/*.mp3) $(wildcard audio/*.aac) $(wildcard audio/*.m4a)
	$(AVS2WAV) $(IN_AVS)  - |$(FFMPEG) -i - $(AOPTION) $(TMPA1)
	

.PHONY: movie-preview
movie-preview: version $(OUT_MP4_PRE)
$(OUT_MP4_PRE): $(TMPA1) $(TMPV1_PRE) $(TMPV2_PRE)
	$(REMUXER) -i $(TMPV2_PRE) -i $(TMPA1) -o $(OUT_MP4_PRE)

$(TMPV2_PRE): $(TMPV1_PRE)
	$(MUXER) -i $(TMPV1_PRE)?fps=$(FPS) -o $(TMPV2_PRE)

$(TMPV1_PRE): $(wildcard *.avs) $(wildcard video/*.avs) $(wildcard video/_output/*.avi) $(VER_AVS)
	$(X264) $(VOPTION) $(VOPT_FAST) -o $(TMPV1_PRE) $(IN_AVS)


.PHONY: version
version:
# gitのリビジョン番号を取得する
	echo 'global VERSION="$(shell git describe --abbrev=7 --dirty --always --tags)"' >  $(VER_AVS).tmp
# リビジョン番号が変化していたときのみ上書きする
	test -e $(VER_AVS) || cp -f $(VER_AVS).tmp $(VER_AVS)
	test "$$(md5sum $(VER_AVS).tmp | awk '{ print $$1 }')" != "$$(md5sum $(VER_AVS) | awk '{ print $$1 }')" && cp -f $(VER_AVS).tmp $(VER_AVS) || true
	rm -f $(VER_AVS).tmp


.PHONY: thumbnails
thumbnails: $(patsubst video/_output/%.avi,_release/img/%.jpg,$(wildcard video/_output/*.avi))
_release/img/%.jpg: $(OUT_DIR)/%.avi
	mkdir -p _release/img
	$(FFMPEG) -i "$<" -vframes 1 -ss 2.0 -f image2 -s 640x360 -y "$@" || true
	test -f "$@" || $(FFMPEG) -i "$<" -vframes 1 -ss 0 -f image2 -s 640x360 -y "$@"


.PHONY: authors-html
authors-html: thumbnails
	$(BASE_DIR)/utils/scripts/make_authors_html_groupby_cut.sh
